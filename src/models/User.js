const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    region: {
        type: DataTypes.STRING,
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },

    gender: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    is_content_creator: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

});

User.associate = function(models) {
    User.hasMany(models.Comment, {});
    User.hasMany(models.FilmList, {});
}

module.exports = User;
