const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Film = sequelize.define('Film', {
    sinopsis: {
        type: DataTypes.STRING,
        allowNull: false
    },

    cast: {
        type: DataTypes.STRING,
        allowNull: false
    },

    release: {
        type: DataTypes.DATE,
        allowNull: false
    },

    duration: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    genre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    recommended_age: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
},{

});

Film.associate = function(models){
    Film.hasMany(models.Comment);
}

module.exports = Film;
