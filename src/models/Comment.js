const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Comment = sequelize.define('Comment', {
    description: {
        type: DataTypes.STRING,
        allowNull: false
    }
},{

});

Comment.associate = function(models){
    Comment.belongsTo(models.User);
    Comment.belongsTo(models.Film);
}

module.exports = Comment;