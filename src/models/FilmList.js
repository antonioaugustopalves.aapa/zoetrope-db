const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const FilmList = sequelize.define('FilmList', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

},{

});

FilmList.associate = function(models){
    FilmList.belongsTo(models.User);
    FilmList.hasMany(models.Film);
}

module.exports = FilmList;
