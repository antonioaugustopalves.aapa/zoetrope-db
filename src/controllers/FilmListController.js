const { response } = require('express');
const FilmList = require('../models/FilmList'); 


const index = async(req,res) => {
    try {
        const film_lists = await FilmList.findAll();
        return res.status(200).json({film_lists});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const film_list = await FilmList.findByPk(id);
        return res.status(200).json({film_list});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
        console.log("teste", req.body);
        const film_list = await FilmList.create(req.body);
        res.status(201).json({film_list: film_list}); 
        }catch(err){
          res.status(500).json({error: err});
        }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await FilmList.update(req.body, {where: {id: id}});
        if(updated) {
            const film_list = await FilmList.findByPk(id);
            return res.status(200).send(film_list);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("FilmListe não encontrado");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await FilmList.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("FilmListe deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("FilmListe não encontrado.");
    }
};

const addRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const film_list = await FilmList.findByPk(id);
        const role = await Role.findByPk(req.body.RoleId);
        await film_list.setRole(role);
        return res.status(200).json(film_list);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const film_list = await FilmList.findByPk(id);
        await film_list.setRole(null);
        return res.status(200).json(film_list);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationRole,
    removeRelationRole
};
