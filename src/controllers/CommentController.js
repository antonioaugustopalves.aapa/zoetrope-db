const { response } = require('express');
const Comment = require('../models/Comment');
const Sequelize = require('../config/sequelize');


const index = async(req,res) => {
    try {
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
          const comment = await Comment.create(req.body);
          return res.status(201).json({message: "Comentário postado com sucesso!", comment: comment});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuario deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

const addRelationComment = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        const user = await Comment.findByPk(req.body.CommentId);
        await comment.setComment(user);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationComment = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setComment(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationComment,
    removeRelationComment
};
