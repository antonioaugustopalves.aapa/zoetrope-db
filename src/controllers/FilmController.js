const { response } = require('express');
const Film = require('../models/Film'); 
const User = require('../models/User');


const index = async(req,res) => {
    try {
        const films = await Film.findAll();
        return res.status(200).json({films});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const film = await Film.findByPk(id);
        return res.status(200).json({film});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
        console.log("teste", req.body);
        const film = await Film.create(req.body);
        res.status(201).json({film: film}); 
        }catch(err){
          res.status(500).json({error: err});
        }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Film.update(req.body, {where: {id: id}});
        if(updated) {
            const film = await Film.findByPk(id);
            return res.status(200).send(film);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Filme não encontrado");
    }
};
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Film.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Filme deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Filme não encontrado.");
    }
};

const addRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const film = await Film.findByPk(id);
        const role = await Role.findByPk(req.body.RoleId);
        await film.setRole(role);
        return res.status(200).json(film);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const film = await Film.findByPk(id);
        await film.setRole(null);
        return res.status(200).json(film);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationRole,
    removeRelationRole
};
