const { Router } = require('express');
const UserController = require('../controllers/UserController');
const FilmController = require('../controllers/FilmController');
const CommentController = require('../controllers/CommentController');
const FilmListController = require('../controllers/FilmListController');
const router = Router();

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);
router.post('/comments', CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', CommentController.destroy);

router.get('/films', FilmController.index);
router.get('/films/:id', FilmController.show);
router.post('/films', FilmController.create);
router.put('/films/:id', FilmController.update);
router.delete('/films/:id', FilmController.destroy);

router.get('/film_list', FilmListController.index);
router.get('/film_list/:id', FilmListController.show);
router.post('/film_list', FilmListController.create);
router.put('/film_list/:id', FilmListController.update);
router.delete('/film_list/:id', FilmListController.destroy);

module.exports = router;
